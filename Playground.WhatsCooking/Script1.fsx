﻿//reference deelde with fsharp charting
#r "../packages/Deedle.1.2.4/lib/net40/Deedle.dll"
#r "../packages/FSharp.Charting.0.90.12/lib/net40/FSharp.Charting.dll"
#I "../packages/FSharp.Charting.0.90.12"
#load "FSharp.Charting.fsx"
#r "../packages/Newtonsoft.Json.7.0.1/lib/net45/Newtonsoft.Json.dll"

open System
open System.IO
open System.Collections.Generic
open Newtonsoft.Json
open FSharp.Charting
open Deedle

type Receipt = 
    { id : int
      cuisine : string
      ingredients : string array }

// read data from file to memory
let path = __SOURCE_DIRECTORY__ + "../../data/What's Cooking/train.json"
let sr = new StreamReader(path)
let json = JsonConvert.DeserializeObject<Receipt array>(sr.ReadToEnd())
let classificationCollection = new Dictionary<string, Dictionary<string, int>>()

json |> Array.iter (fun receipt -> 
            // truncate all ingredients
            receipt.ingredients |> Array.iter (fun ing -> 
                                       match classificationCollection.ContainsKey(ing) with
                                       | true -> 
                                           // check cuisine
                                           match classificationCollection.[ing].ContainsKey(receipt.cuisine) with
                                           | true -> 
                                               classificationCollection.[ing].[receipt.cuisine] <- classificationCollection.[ing].[receipt.cuisine] 
                                                                                                   + 1
                                           | _ -> classificationCollection.[ing].Add(receipt.cuisine, 1)
                                       | _ -> 
                                           let cuisineDict = new Dictionary<string, int>()
                                           cuisineDict.Add(receipt.cuisine, 1)
                                           classificationCollection.Add(ing, cuisineDict)))
classificationCollection.["baking powder"].Values

// open test collection
let pathTest = __SOURCE_DIRECTORY__ + "../../data/What's Cooking/test.json"
let srTest = new StreamReader(pathTest)
let jsonTest = JsonConvert.DeserializeObject<Receipt array>(srTest.ReadToEnd())

// clasify 
let clasify (ingredients : string array) (collection : Dictionary<string, Dictionary<string, int>>) = 
    let outcome = new Dictionary<string, double>()
    ingredients |> Array.iter (fun ing -> 
                       match collection.ContainsKey(ing) with
                       | true -> 
                           let sum = 
                               collection.[ing].Values
                               |> Seq.sum
                               |> double
                           collection.[ing] |> Seq.iter (fun (KeyValue(k, v)) -> 
                                                   match outcome.ContainsKey(k) with
                                                   | true -> 
                                                       outcome.[k] <- outcome.[k] + (double collection.[ing].[k]) / sum
                                                   | _ -> outcome.Add(k, (double collection.[ing].[k]) / sum))
                       | _ -> 0 |> ignore)
    outcome
    |> Seq.sortByDescending (fun (KeyValue(_, v)) -> v)
    |> Seq.map (fun (KeyValue(k, v)) -> (k, v))
    |> Seq.head
    |> fst

let outcomePath = __SOURCE_DIRECTORY__ + "../../data/What's Cooking/submission.csv"
let swSubmission = new StreamWriter(outcomePath, false)

swSubmission.WriteLine("id,cuisine")
jsonTest |> Array.iter (fun rep -> 
                let outcome = clasify rep.ingredients classificationCollection
                printfn "Id: %A" rep.id
                printfn "Outcome: %A" outcome
                printfn ""
                swSubmission.WriteLine(rep.id.ToString() + "," + outcome) |> ignore)
swSubmission.Flush()
swSubmission.Close()

//    let measures = 
//        ingredients
//        |> Array.map(fun ing -> 
//            let sum = collection.[ing].Values |> Seq.sum
//            collection.[ing], sum)
//    
//    measures 
//        |> Array.iter(fun (cuisines,sum) ->
//            [
//                for cus in cuisines ->
//                    cus.Key, sum/cus.Value
//            ]
//        )
// create list of all possible ingredients/cuisines
let ingridients = new Dictionary<string, int>()
let cuisines = new Dictionary<string, int>()

json |> Array.iter (fun receipt -> 
            // check cuisine
            match cuisines.ContainsKey(receipt.cuisine) with
            | true -> cuisines.[receipt.cuisine] <- cuisines.[receipt.cuisine] + 1
            | _ -> cuisines.Add(receipt.cuisine, 1)
            // check ingredients
            receipt.ingredients |> Array.iter (fun ing -> 
                                       match ingridients.ContainsKey(ing) with
                                       | true -> ingridients.[ing] <- ingridients.[ing] + 1
                                       | _ -> ingridients.Add(ing, 1)))

// what is most popular
let orderedInredients = 
    ingridients
    |> Seq.sortByDescending (fun (KeyValue(_, v)) -> v)
    |> Seq.map (fun (KeyValue(k, v)) -> (k, v))
    |> Seq.take (10)

Chart.Column(orderedInredients).WithDataPointLabels()

let orderedCousines = 
    cuisines
    |> Seq.sortByDescending (fun (KeyValue(_, v)) -> v)
    |> Seq.map (fun (KeyValue(k, v)) -> (k, v))

Chart.Column(orderedCousines).WithDataPointLabels()

// create data table rows receipt id, columns all ingredients 
(* OutOfMemory approach 
let columns = ingredients |> Seq.map(fun (KeyValue(k,_)) -> k) |> Seq.toArray

let values = 
    [
        for col in columns ->
            Series.ofValues 
                [ 
                    for rep in json ->
                        let res = rep.ingredients |> Array.tryFind(fun ing -> ing = col)
                        match res with
                        | Some _ -> true
                        | _ -> false
                ]
    ]

*)

(* 
    Reducing columns by hand, keep just 50 most occurring ingredients
    Pushing all values into matrix and data frame, column are indexing by numbers, 
    in the same order as in columns collection
*)

let columns = 
    ingridients
    |> Seq.map (fun (KeyValue(k, _)) -> k)
    |> Seq.take (50)

let values = 
    array2D [ for col in columns -> 
                  [ for rep in json -> 
                        let res = rep.ingredients |> Array.tryFind (fun ing -> ing = col)
                        match res with
                        | Some _ -> true
                        | _ -> false ] ]

let cookingFrame = Frame.ofArray2D values

// adding cuisine column
let allCuisines = 
    json
    |> Array.map (fun js -> js.cuisine)
    |> Series.ofValues

cookingFrame.AddColumn(-1, allCuisines)

//let testFrame = Frame(columns,)
//    frame [""]
let idFrame = Frame.ofRecords json |> Frame.indexRowsString "id"
