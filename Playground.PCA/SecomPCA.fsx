﻿// reference accord framework
#r "../packages/Accord.3.0.2/lib/net45/Accord.dll"
#r "../packages/Accord.Controls.3.0.2/lib/net45/Accord.Controls.dll"
#r "../packages/Accord.IO.3.0.2/lib/net45/Accord.IO.dll"
#r "../packages/Accord.Math.3.0.2/lib/net45/Accord.Math.dll"
#r "../packages/Accord.Statistics.3.0.2/lib/net45/Accord.Statistics.dll"
//reference deelde with fsharp charting
#r "../packages/Deedle.1.2.4/lib/net40/Deedle.dll"
#r "../packages/FSharp.Charting.0.90.12/lib/net40/FSharp.Charting.dll"
#I "../packages/FSharp.Charting.0.90.12"
#I "../packages/Deedle.1.2.4"
#load "FSharp.Charting.fsx"
#load "Deedle.fsx"

open Deedle
open System
open Accord
open Accord.Controls
open Accord.Math
open Accord.Math.Comparers
open Accord.Math.Decompositions
open Accord.Statistics
open Accord.Statistics.Analysis
open FSharp.Charting

let frame1 = Frame.ReadCsv(__SOURCE_DIRECTORY__ + "../../data/secom.data.txt", hasHeaders = false, separators = " ")


frame1.ColumnKeys
|> Seq.map(
    fun key -> 
        let column = frame1.GetColumn(key)
        // compure mean
        let mean = Stats.mean column
        (key,mean))
|> Seq.iter(fun (key, mean) ->
    let column = frame1.GetColumn(key)
    let newColumn = column.FillMissing mean
    frame1.ReplaceColumn(key, newColumn))


// get data form frame
let matrix : float [,] = frame1.ToArray2D()

let pca = new PrincipalComponentAnalysis(matrix)

// Also we can set to use the analysis by correlation, which is more indicated when analysing data with high different measurement units
pca.Method = AnalysisMethod.Standardize

// and just compute
pca.Compute();

let totalSum = pca.Eigenvalues.Sum()
let variances =
 [
    for i in 0..pca.Eigenvalues.Length - 1 -> (i,(pca.Eigenvalues.[i]/totalSum * 100.0))
 ]

Chart.Line(variances).WithXAxis(Max=20.0)

// transform data 
let pcaFinalData = pca.Transform(matrix)


// Substract mean
let meanSVD = matrix.Mean()
meanSVD |>
    Array.iteri(fun index item ->
        if Double.IsNaN(item) then
            printfn "Nan index %A" index
    )

let dataAdjustSVD = matrix.Subtract(meanSVD)

// We are skipping calculation of covariance matrix and execute SVD
let svd = new SingularValueDecomposition(dataAdjustSVD)

let singularValues = svd.Diagonal
let svdEigenVectors = svd.RightSingularVectors

// Calculate the eigen values as the square of the singular values
let svdEigenValuesTempStep = singularValues.ElementwisePower(2.0)

// because of SVD we need divide eigen values by n - 1 to get the same as from EigenValueDecomposition
let svdEigenValues = svdEigenValuesTempStep.Divide(float (matrix.GetLength(0)) - 1.0)

printfn "Feature vector %A" (svdEigenVectors.ToString(" +0.0000000000; -0.0000000000;"))

// create final data for SVD
let svdFinalData = dataAdjustSVD.Multiply(svdEigenVectors)

printfn "%A" (svdFinalData.ToString("  0.0000000000; -0.0000000000;"))



//|> Frame.getCol("Column1")
|> Series.fillMissingUsing (fun k -> 
       match k with
       | "<missing>" -> ObjectSeries(0))
frame1.GetColumn("Column1")

let res : string seq = frame1.GetAllValues()

res

let columns = frame1.GetAllColumns()

// we need to replace missing values by means
//let means = 
//    [| for i in 0..frame.ColumnCount -> frame.GetColumn() |]
let aF = 
    frame [ "AID" =?> Series.ofValues [ "Andrew"; "Andrew"; "Andrew" ]
            "AMES" =?> Series.ofValues [ 2; 4; 3 ] ]
    
let bF = 
    frame [ "BID" =?> Series.ofValues [ "Andrew"; "Chris"; "Andrew" ]
            "BMES" =?> Series.ofValues [ 1; 6; 7 ] ]
    
let groupF = 
    frame [ "AG" => (aF
                        |> Frame.groupRowsByString "AID"
                        |> Frame.getCol "AMES")
            "BG" => (bF
                        |> Frame.groupRowsByString "BID"
                        |> Frame.getCol "BMES") ]
    
let groupFMean = 
    groupF
    |> Frame.getNumericCols
    |> Series.mapValues (Stats.levelMean fst)
    |> Frame.ofColumns
    |> Frame.fillMissingWith 0
    
groupFMean.SaveCsv("tgroupFMean.csv", includeRowKeys = true, keyNames = [ "Id" ])
